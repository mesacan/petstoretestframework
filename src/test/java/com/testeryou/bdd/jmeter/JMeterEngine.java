package com.testeryou.bdd.jmeter;

import com.testeryou.bdd.generic.ProjectProperties;
import org.apache.commons.io.FileUtils;
import org.apache.jmeter.engine.StandardJMeterEngine;
import org.apache.jmeter.report.dashboard.ReportGenerator;
import org.apache.jmeter.reporters.ResultCollector;
import org.apache.jmeter.reporters.Summariser;
import org.apache.jmeter.save.SaveService;
import org.apache.jmeter.util.JMeterUtils;
import org.apache.jorphan.collections.HashTree;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Objects;
import java.util.logging.Logger;

import static com.testeryou.bdd.generic.PropertyNames.JMETER_LOCATION;

public class JMeterEngine {

    private static final Logger LOG = Logger.getLogger(JMeterEngine.class.getName());
    private static StandardJMeterEngine jMeterEngine;
    protected static HashTree testPlanTree;
    private static File testPlanFile;
    private static File jmeterHomePath;
    private static String reportDirectory = "target/PerformanceReport/";
    private static Summariser summer;

    private static ReportGenerator reportGenerator;

    //This method will load and Identify JMeter in your system.
    public static void loadJMeterConfig() {
        testPlanTree = new HashTree();
        jmeterHomePath = new File(ProjectProperties.getJMeterProperties().getProperty(JMETER_LOCATION));
        if (jmeterHomePath.exists()) {
            JMeterUtils.setJMeterHome(jmeterHomePath.getPath());
            JMeterUtils.loadJMeterProperties(jmeterHomePath.getPath() + File.separator + "bin" + File.separator + "jmeter.properties");
            JMeterUtils.initLocale();
            LOG.info("JMeter Initialized successfully");
        } else {
            LOG.severe("JMeter does not set, please check path of the apache location in property file");
            System.exit(1);
        }
    }

    public static void executeTestWithJMXFile(String reportName, String jmxFilePath) {
        try {
            testPlanFile = new File(System.getProperty("testPlan.location", jmxFilePath));
            SaveService.loadProperties();
            testPlanTree = SaveService.loadTree(testPlanFile);
            LOG.info("Run and Generate Report");
            generateReport(reportName);
        } catch (Exception e) {
            LOG.severe(e.getMessage());
            System.exit(1);
        }
    }

    public static void generateReport(String reportName) {
        try {
            JMeterUtils.setProperty("jmeter.reportgenerator.exporter.html.property.output_dir", reportDirectory + reportName);
            String summariserName = JMeterUtils.getPropDefault("summariser.name", "summary");
            if (!summariserName.isEmpty()) {
                summer = new Summariser(summariserName);
            }
            File report = new File(reportDirectory + reportName);
            File reportFile = new File(reportDirectory + reportName + "_JTL_Results" + "\\result.jtl");
            if (report.exists()) {
                flushDirectory(report);
                LOG.info("Report folder deleted");
                if (reportFile.exists()) {
                    boolean delete1 = reportFile.delete();
                    LOG.info("Report File deleted" + delete1);
                }
            }
            ResultCollector logger = new ResultCollector(summer);
            reportGenerator = new ReportGenerator(reportFile.getPath(), logger);
            logger.setFilename(reportFile.getPath());
            testPlanTree.add(testPlanTree.getArray()[0], logger);
            jMeterEngine = new StandardJMeterEngine();
            jMeterEngine.configure(testPlanTree);
            LOG.info("Performance Execution Started..........");
            FileUtils.deleteDirectory(new File("./report-output"));
            jMeterEngine.run();
            reportGenerator.generate();
            LOG.info("Report Generated Successfully");
        } catch (Exception e) {
            LOG.severe(e.getMessage());
        }
    }

    public static void flushDirectory(File directoryName) throws IOException {
        FileUtils.cleanDirectory(directoryName);
    }
    public static File getJson(String xmlName){
        try {
            return new File(Objects.requireNonNull(JMeterEngine.class.getClassLoader().getResource("./jsonFiles/" + xmlName + ".json")).toURI());
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }

    }
}
