package com.testeryou.bdd.steps;


import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.apache.commons.io.FileUtils;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import static com.testeryou.bdd.jmeter.JMeterEngine.*;
import static com.testeryou.bdd.jmxfile.XMLFileUpdater.*;

public class PerformancePetSteps {

    @Given("user updates the following values in the given {string} JMX file")
    public void userUpdatesTheFollowingValuesInTheGivenJMXFile(String fileName, DataTable threadGroup) throws IOException {
        loadJMeterConfig();
        List<Map<String, String>> threadList = threadGroup.asMaps(String.class, String.class);
        if (!threadList.get(0).get("LoopController").isEmpty())
            updateElementText(loop,
                    jmxFILEPath + fileName, 0, threadList.get(0).get("LoopController"));
        if (!threadList.get(0).get("RampUpTime").isEmpty())
            updateElementText(Rampup,
                    jmxFILEPath + fileName, 0, threadList.get(0).get("RampupTime"));
        if (!threadList.get(0).get("NoOfThreads").isEmpty())
            updateElementText(threads,
                    jmxFILEPath + fileName, 0, threadList.get(0).get("NoOfThreads"));
        if (!threadList.get(0).get("JsonFile").isEmpty())
            updateElementText(requestBody,
                    jmxFILEPath + fileName, 0,
                    FileUtils.readFileToString(getJson(threadList.get(0).get("JsonFile")), "UTF-8"));
    }

    @Then("user execute a performance test by supplying JMX file {string} and generate report {string}")
    public void userExecuteAPerformanceTestBySupplyingJMXFileAndGenerateReport(String fileName, String reportName) {
        executeTestWithJMXFile(reportName, jmxFILEPath + fileName);
    }
}
