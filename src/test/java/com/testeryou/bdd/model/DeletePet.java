package com.testeryou.bdd.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DeletePet  implements SerializableJson{
    String message;
    String type;
}
