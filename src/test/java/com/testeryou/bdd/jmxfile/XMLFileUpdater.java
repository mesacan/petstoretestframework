package com.testeryou.bdd.jmxfile;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import java.io.File;

public class XMLFileUpdater {
    private static Document doc;
    private static XPath xPath;
    private static NodeList nodes;
    private static Transformer xformer;

    public static String jmxFILEPath="src/test/resources/jmxfiles/";
    public static String loop="//hashTree/ThreadGroup/elementProp/stringProp[@name='LoopController.loops']";
    public static String threads="//hashTree/ThreadGroup/stringProp[@name='ThreadGroup.num_threads']";
    public static String Rampup="//hashTree/ThreadGroup/stringProp[@name='ThreadGroup.ramp_time']";
    public static String requestBody="//hashTree/HTTPSamplerProxy/elementProp/collectionProp/elementProp/stringProp[@name='Argument.value']";

    public static void updateElementText(String givenXpath, String filePath, int itemNo, String modifiedContent) {
        try {
            doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(
                    new InputSource(filePath));
            xPath = XPathFactory.newInstance().newXPath();
            nodes = (NodeList) xPath.evaluate(givenXpath, doc,
                    XPathConstants.NODESET);
            nodes.item(itemNo).setTextContent(modifiedContent);
            xformer = TransformerFactory.newInstance().newTransformer();
            xformer.transform(new DOMSource(doc), new StreamResult(new File(filePath)));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
