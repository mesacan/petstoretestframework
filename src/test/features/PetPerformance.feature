Feature: This example is about how to run simple http load testing for pet store pet configuration

  @JmxFile
  Scenario: To run performance test for pet overload traffic
    Given user updates the following values in the given "PetLoadTest.jmx" JMX file
    | NoOfThreads | RampUpTime | LoopController | JsonFile  |
    | 1           | 1          | 1              | CreatePet |
    Then user execute a performance test by supplying JMX file "PetLoadTest.jmx" and generate report "JMX_Test_Results"