# Pet Store API Test Automation Document

Functional testing of an API involves verifying that the API functions according to its specification and meets the users' needs. 

For the Swagger PetStore API (a sample API provided by Swagger for demonstration purposes), an appropriate scope for functional testing should cover the key functionalities as documented in its Swagger page. 

Here is a breakdown of areas to focus on, based on typical operations available in the Petstore API:

* User Operations
* Pet Operations
* Store Operations

I would like to focus on the pet operations in this project.

## Pet Operations

* **Add a New Pet**: Test adding pets with various combinations of attributes (e.g., status, categories, tags).
* **Update an Existing Pet**: Verify updating pet details works correctly, including status and other attributes.
* **Get Pet by ID**: Test retrieving a pet's details by its ID.
* **Delete a Pet**: Ensure the API correctly handles pet deletion.

# Testing Strategies
**Positive Testing**: Test with valid input data to ensure the API behaves as expected.

**Negative Testing**: Use invalid inputs (e.g., incorrect data types, out-of-range values) to test how the API handles errors.

**Boundary Testing**: Test the limits of input fields (e.g., string length, maximum and minimum values for numbers) to ensure the API handles them gracefully.

**Security Testing**: Ensure that the API properly secures sensitive endpoints, requiring authentication where necessary and rejecting unauthorized requests.

**Performance Testing**: Although not strictly functional testing, assessing how the API handles a high load of requests is important for ensuring it performs well under stress.

In this project, you will see the positive and performance testing examples.

# How It Works

## For local Usage

You need to install maven to the pc's path.

You have to go to the folder that you clone the project.

Then you can run command below to run the pet configuration tests:

```
mvn clean test -Dcucumber.filter.tags="@PetFunctions"
```

For the performance test you need to install apache-jmeter to your local pc and 
give the bin folder path into the loadtest.properties file.

Then you can run the tests with command below:
```
mvn clean test -Dcucumber.filter.tags="@JmxFile"
```

You can have the result html, json and Junit XML in the target/cucumber-report and PetPerformance folder for performance tests.

OR

You can download intellij idea with plugins below:
* Gherkin
* Cucumber for Java
* Cucumber+

You can execute feature file in the IDE.

## Usage of Gitlab CI

You can check the .gitlab-ci.yml file for the pipeline created.

For each code push pipeline will be triggered.